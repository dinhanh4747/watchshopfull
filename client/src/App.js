import React from 'react';
import {Route, BrowserRouter, Switch} from "react-router-dom"
import Header from './components/Header';
import Cart from './pages/Cart';
import Detail from './pages/Detail';
import Home from './pages/Home';
import Signin from './pages/Signin';
import Signup from './pages/Signup';

const App = () => {
  return (
    <BrowserRouter>
      <Header/>
      <Switch>  
        <Route path="/detail/:id" component={Detail}/>
        <Route path="/signin" component={Signin}/>
        <Route path="/signup" component={Signup}/>
        <Route path="/cart" component={Cart}/>
        <Route path="/" component={Home}/>
      </Switch>
    </BrowserRouter>
  );
};

export default App;