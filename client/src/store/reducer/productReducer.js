import { actionType } from "../action/type";

const initialState = {
    productList : []
}

const reducer = (state = initialState , {type , payload}) => {
    switch(type) {
        case actionType.SET_PRODUCT : {
            state.productList = payload;
            return {...state};
        }
        default : return {...state};
    }
}

export default reducer;