import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";

export const signUpAction = (userSignup) => {
    return async (dispatch) => {
        try {
            const res = await axios({
                method : "POST",
                url : "http://localhost:5000/users",
                data : userSignup,
            })
            dispatch(createAction(res.data));
            console.log("data sign up", res.data);
        } catch (error) {
            console.log(error);
        }   
    }
}

export const signInAction = (userSignin, callback) => {
    return async (dispatch) => {
        try {
            const res = await axios({
                method : "POST",
                url : "http://localhost:5000/users/signin",
                data : userSignin,
            })
            dispatch(createAction(actionType.SET_USER,res.data));
            console.log("data sign in", res.data);
            callback()
        } catch (error) {
            console.log(error.message);
        }
    }
}