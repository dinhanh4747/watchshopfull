import axios from "axios"
import { createAction } from "."
import { actionType } from "./type"

export const setProductAction = () => {
    return async (dispatch) => {
        try {
            const res = await axios({
                method : "GET",
                url : "http://localhost:5000/products",
            })
            dispatch(createAction(actionType.SET_PRODUCT,res.data))
            console.log("data product",res.data);
            
        } catch (error) {       
            console.log(error);
        }
    }
}