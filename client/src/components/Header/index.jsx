
import React from "react";
// import {Navbar, Nav, Form, FormControl, Button} from "react-bootstrap"
import { NavLink } from "react-router-dom";
import "./header.css"

const Header = () => {
  return (
    <div style={{position : "fixed", width : '100%' , zIndex : 99, marginBottom : 100}}>
      <nav className="navbar navbar-expand-lg navbar-light bg-dark">
        <NavLink to="/"  className="navbar-brand text-light" >
          Watch Shop
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto ">
            <li className="nav-item active">
              <NavLink to="/" className="nav-link text-light" >
                Home <span className="sr-only">(current)</span>
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/"  className="nav-link text-light" >
                Contact
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/"  className="nav-link text-light" >
                News
              </NavLink>
            </li>



          </ul>
          <form className="form-inline my-2 my-lg-0">
            <input
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button
              className="btn btn-outline-success my-2 my-sm-0"
              type="button"
            >
              Search
            </button>
          </form>
        
        </div>
        <div className="d-flex">
            <NavLink to="/signin" className="nav-link text-light" activeClassName="active">Sign in</NavLink>
            <NavLink to="/signup" className="nav-link text-light" activeClassName="active">Sign up</NavLink>
          </div>
      </nav>
    </div>
  );
};

export default Header;
