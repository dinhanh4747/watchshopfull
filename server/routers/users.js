import express from 'express';
import { getUsers, createUser, updateUser } from '../controllers/users.js';
import { UserModel } from "../models/UserModel.js";
import argon2 from 'argon2';
import jwt from 'jsonwebtoken';


const router = express.Router();
//http://localhost:5000/users

router.get('/', getUsers);

router.post('/', createUser);

router.post('/update', updateUser);

router.post('/signup', async (req, res) => {

        const { fullName , username, password, phoneNumber, email, address } = req.body;

        if (!fullName || !username || !password || !phoneNumber || !email || !address )
            return res.status(400).json({ success: false, message: "Nhap thieu thong tin" });
        try {
        //check user
        const user = await UserModel.finfOne({ username })

        if (user)
            return res.status(400).json({ success: false, message: "username da ton tai" })
        const hashedPassword = await argon2.hash(password)
        const newUser = new UserModel({ fullName, username, hashedPassword, phoneNumber, email , address});
        await newUser.save();
        // token
        const accessToken = jwt.sign({ userId: newUser._id }, process.env.ACCESS_TOKEN);
        res.json({ success: true, message: "Dang ki thanh cong", newUser , accessToken })
    } catch (error) {
    }
})
router.post('/signin', async (req, res) => {
    const { username, password } = req.body;
    try {
        //check user
        const user = await UserModel.findOne({ username })
        if (!user)
            return res.status(400).json({ success: false, message: "sai ten dang nhap hoac mat khau" })

        const passwordValid = await argon2.verify(user.password, password);
        if (!passwordValid)
            return res.status(400).json({ success: false, message: "sai ten dang nhap hoac mat khau" })


        const accessToken = jwt.sign({ userId: user._id }, process.env.ACCESS_TOKEN); 
        res.json({ success: true, message: "Dang nhap thanh cong", accessToken })
    } catch (error) {
        
    }
})


export default router;
