import { UserModel } from "../models/UserModel.js";
import jwt from 'jsonwebtoken';
import argon2 from 'argon2';

export const getUsers = async (req, res) => {
  try {
    const users = await UserModel.find();
    console.log("users", users);
    res.status(200).json(users);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const createUser = async (req, res) => {
  try {

  // const newUser = req.body; 

  // const user = new UserModel(newUser);

  // // token
  // const accessToken = jwt.sign({ userId: user._id }, process.env.ACCESS_TOKEN);

  // res.json({ success: true, message: "Dang ki thanh cong", user, accessToken });

  // await user.save();
  // // res.status(20 const { fullName , username, password, phoneNumber, email, address } = req.body;
  const { fullName , username, password, phoneNumber, email, address } = req.body;

  if (!fullName || !username || !password || !phoneNumber || !email || !address)
    return res.status(400).json({ success: false, message: "Nhap thieu thong tin" });

  // try {
    //check user
    const user = await UserModel.finfOne({ username })

    if(user)
      return res.status(400).json({ success: false, message: "username da ton tai" })
    const hashedPassword = await argon2.hash(password)
    const newUser = new UserModel({ fullName, username, hashedPassword, phoneNumber, email, address });
    await newUser.save();
    // token
    const accessToken = jwt.sign({ userId: newUser._id }, process.env.ACCESS_TOKEN);
    res.json({ success: true, message: "Dang ki thanh cong", newUser, accessToken })


  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const updateUser = async (req, res) => {
  try {
    const updateUser = req.body;

    const user = await UserModel.findOneAndUpdate(
      { _id: updateUser._id },
      updateUser,
      { new: true }
    );

    res.status(200).json(user);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};
