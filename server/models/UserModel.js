import mongoose from "mongoose";

const schema = new mongoose.Schema(
  {
    fullName: {
      type: String,
      required: true,
    },
    username: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    phoneNumber: {
      type: String,
      required: true,
      unique: true,
    },
    email: {
      type: String,
      required: "Email address is required",
      unique: true,
      validate: {
        validator: function (v) {
            return this.model("User")
                .findOne({ email : v })
                .then((email) => !email);
        },
        message: (props) => `${props.value} is already registered`,
    },
      
    },
    address: {
      type: String,
      required: true,
    },
    userType: {
      type: String,
      default: "Customer",
    },
  },
  { timestamps: true }
);

export const UserModel = mongoose.model("User", schema);
